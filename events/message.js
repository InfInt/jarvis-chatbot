const kick =   require("../commands/kick.js");
const tiktok = require("../commands/tiktok.js");
const helpIssue = require("../help/issue.js");

module.exports = (client, message) => {
  if (message.content.startsWith("!kick")) {
    return kick(message)
  }
  if (message.content.includes("https://vm.tiktok.com/") ) {
    return tiktok(message)
  }
  if (message.content.startsWith("issue?")) {
    return helpIssue(message)
  }
}
