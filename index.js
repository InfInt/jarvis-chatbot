require("dotenv").config()
const Sentry = require('@sentry/node');
const Discord = require("discord.js")

Sentry.init({
  release: 'jarvis-bot@' + process.env.npm_package_version
});

const client = new Discord.Client()
const fs = require("fs")

fs.readdir("./events/", (err, files) => {
  files.forEach(file => {
    const eventHandler = require(`./events/${file}`)
    const eventName = file.split(".")[0]
    client.on(eventName, (...args) => eventHandler(client, ...args))
  })
})

client.login(process.env.BOT_TOKEN)
