
module.exports = (message) => {
  message.author.send("You can automatically create an issue for J.a.v.i.s. Just type 'new issue' as direct message to me and I will guide you through the process.")
}
