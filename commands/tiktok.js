var util = require("util"),
    http = require("https");
const $ = require('cheerio');
const axios = require('axios');
const Discord = require("discord.js");
const fs = require("fs")
var attachment;

module.exports = message => {
  var startPos = message.toString().indexOf("https://vm.tiktok.com/");
  var vidID;
  var vidURL;
  if (startPos != -1) {
    var slashPos = startPos + 22;
    if (message.toString().indexOf("/",startPos+22) != -1) {
      slashPos = message.toString().indexOf("/",startPos+22)
    } else if (message.toString().indexOf(" ",startPos+22) != -1) {
      slashPos = message.toString().indexOf(" ",startPos+22)
    } else {
      slashPos = message.toString().length -1
    }

    vidID = message.toString().substring(startPos+21,slashPos+1);
    console.log("vidID: "+ vidID);
  }

  axios.get("https://vm.tiktok.com" + vidID, {
              config: {
                responseType: "text"
              },
              headers: {
                'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36"
              }
  })
    .then(function(html){
      var urlTagPos = html.data.indexOf("urls")
      if (urlTagPos != -1) {
        urlTagPos += 7
        var urlTagEnd = html.data.indexOf('"', urlTagPos+1)
        vidURL = html.data.substring(urlTagPos+1, urlTagEnd)

      }


      axios.get(vidURL, {
        responseType: 'arraybuffer',
        headers: {
          'Content-Type': 'video/mp4',
          'Accept': 'video/mp4',
          'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36"
        }
      })
      .then(function(html){
        fs.writeFileSync("tiktok.mp4",html.data);
        attachment = new Discord.MessageAttachment("./tiktok.mp4");
        message.channel.send(attachment)
      })
      .catch(function(err){
        console.log("Err SteamData: "+ err)
      });
    })
    .catch(function(err){
      console.log("Err: "+ err)
    });
}
